---
title: Why I want to join Basecamp.
date: "2019-07-01T22:40:32.169Z"
text: ‘5 min read’
description: Customer support @Basecamp
---

I've always wanted to start a blog, to share my passions, thoughts and above all
engage in conversation with others. Since I am using less and less social medias I've decided to give it a try.
This application is a great opportunity to start, it's adressed to Basecamp recruiting team as much as to potential future applicants or just to satisfy your curiosity :)

Months ago, I wouldn't have believed you if you'd told me I'd apply for that kind of job. After all, I graduated as a marine biologist and recently worked as a web developer..opposite backgrounds. So why switching again and move toward customer services? 

Well it's a long story and it surely needs another blog post to dive into the details but let's just say that I missed interacting with/helping other human beings. Working behind a computer to ship code or to analyze field datastes is interesting but frustrating in a sense that I felt alone, I couldn't see the impact of my work. At the end of the day it didn't feel rewarding, satisfying.


### Why Basecamp customer team


 Openings are rare at Basecamp but applying for a post that does not fit your skills can prove [counterproductive](https://m.signalvnoise.com/dont-buy-the-hiring-lottery/). I've applied to may jobs during last two years but The hiring lottery proved inefficient: 
 
 ![Chinese Salty Egg](./salty_egg.jpg). 
 Having learned my lesson I didn't want to apply in a flury so I selected few companies based on their product, feedback of previous employees, their core value and transparency regerding working conditions, missions. It also became obvious that I could make a better of use my techincal skills at the service of customers and therefore get the best of both worlds. Customer support is a perfect way to combine the satisfaction of clients and my own without feeling selfish. Most importantly, it's awesome for me because I can also use my previous experiences (ex...)on a daily basis which was not the case as a developer.

 One company even made me an offer yesterday but they don't compare with basecamp so I still decided to apply. The pros in favor of Basecamp outweight the cons when comparing with this other company (namely Lightspeed HQ). Don't get me wrong, this a good opportunity. Indeed, the job mission matches my expectations, salary is ok, the team seems nice and benefits are above average. But one the other hand, remote working is not an option, working hours are vague which is not a good sign, and there not a lot of flexibility in terms of life balance (ex..)


 Transparency at Basecamp is kind of an exception, I have never seen this before in a company. It means and tells a lot. It means trust in the process and the people working there. It tells a real volonte to involve everyone and take into consideration evyrone's opinion. What We Stand For of the gudelines is quite clair et net. inspiring cea unlike jobs, zuck etc.

### More technical answer for the recruiters:

- a description of a great customer service/support experience you had recently, and what made it great:

Yesterday I got my road bicycle sent by my parents from France to Montreal where I live at the moment. I appreciated the help provided the airline (AirTransat) who took care of my beloved bike. I arranged the transfer by phone while in Canada,then my dad packed the biked and dropped it at the Airport. I just needed to pick it up the next day at Montreal airport.
It took a ten minutes phone call to arrange the shipment, 15minutes for my dad to do the paperwork back in France and 24h later it was a done task. The pricing was fair, timing matched what I needed and support agent where absolutely professional, they new exactly what to do and gave me all the details. As a customer this is what I love. Reactivity, precision and clarte. No bs and false promise. Hats off Air Transat!


a time you taught yourself a new skill to complete a job or project.

I learned a Javascript library recently (React). Although it was not my first choice I had fun learning it since it was useful for my previous job. In general I try to learn by reading tutorials (a little) and by getting my hands in the dirt (a lot). Outside of job I love to learn skills by experimenting. My last personnal project was the assembly of my road bike from scratch, pieces by pieces. It was fun because at first is like Lego, and then it's fine tuning and reglages des details.

a guide to making your favorite meal. 

I'm gonna cheat a little bit :) I always have a book of my mom's recipes with me so I'll just do some copypasta and translate from French. 

In order to make chorba soup you need (for 6 persons): 


- 400g of diced meat(chicken or lamb)
- 400g of khritaraki/Risoni pasta
- 4 ripped tomatoes
- 1 diced zucchini
- 2 diced carrots
- 1 minced onion
- a handfull of chickpeas
- a bunch of coriander
- half table spoon of cinnamon
- 4 table spoons of olive oil
- Salt and pepper according to your taste


Preparation:

It is very easy, all you need to do is put all your ingredients in a pressure cooker, cover with 1.5l of water and let
it cook for 30 min. 10 minutes before the end of add pastas. Now that you soup is done, remove the coriander bunch, add fresh coriander leafs fresh lemon juice et voila! I personnaly like having my soup with 


Then, pick three of the customer questions below and answer them like you would if you worked here (hint: at this point, we value tone and style over correctness):

    Does Basecamp 3 offer time tracking?

Hello John, thanks for using Basecamp. 

Time tracking is offered via third party [applications](https://basecamp.com/extras#tracking-invoicing-accounting). There' actually quite a lot of them depending on your needs.

Best regards,

Jamil and the Basecamp Team.


    Can I create recurring events in the Basecamp calendar? What about recurring todo items?

Hello Joel, thanks for using Basecamp. 

You can indeed create recurring events in our calendar.  I invite you to kindly have a look
at our blog explaining how to do it: [recurring events](https://m.signalvnoise.com/new-in-basecamp-recurring-events)

Regarding recurring todo items, we do not provide built-in integration. Still, third party applications might help you get around specific [tasks](https://basecamp.com/extras). In your case, [zapier](https://zapier.com/apps/basecamp3/integrations) might be useful.

Best regards,

Jamil and the Basecamp Team.


    Do you offer 2fa for signing in?

Hello Sandra, thanks for using Basecamp. 

Yes, we do provide 2fa sign-in.  At Basecamp we do value your security and privacy. Depending on your version of our product you will find in our blog all the informations required to take full advantage of this feature: [2fa sign-in](https://m.signalvnoise.com protect-your-basecamp-login-with-googles-two-factor-authentication/)

Jamil and the Basecamp Team.
